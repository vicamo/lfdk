Linux Firmware Debug Kit
========================
Linux Firmware Debug Kit (lfdk) is a tool to help debug x86 firmware. It allows one to view memory space, I/O space and PCI space which facilitates debugging firmware/resource settings.  lfdk has a very similar user interface to the DOS "RU" memory editor.

History
-------

lfdk was firstly hosted on [SourceForge](http://sourceforge.net/projects/lfdk/) and was later moved to [GitHub](https://github.com/merckhung/lfdk1) with all git history rewritten. This packaging is based on the latter. As there is neither tags nor any versioning info, the version number was set to epoch of the last upstream commit date.

Install
-------
To install latest, unreleased lfdk from CI builds, download the job [artifacts](https://gitlab.com/vicamo/lfdk/-/jobs/artifacts/debian/sid/download?job=build) and find installable deb package under extracted `debian/output/build` folder.

To install lfdk for Ubuntu:
```bash
$ sudo add-apt-repository ppa:vicamo/ppa
$ sudo apt update && sudo apt install lfdk lfdd-dkms
```
